import { Component } from '@angular/core';

import { LocalNotifications } from '@ionic-native/local-notifications';
import {AlertController, NavController, Platform} from "ionic-angular";
declare var cordova;
@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'hello-ionic.html'
})
export class HelloIonicPage {
  scheduled = [];
  clickSub: any;
  // private cordova: any;
  constructor(private localNotifications: LocalNotifications, public navctrl: NavController, public alerCtrl: AlertController,
              public plt: Platform ) {
    this.plt.ready().then((rdy)=> {
      cordova.plugins.notification.on('click',
        (notification, state) => {
        let json =  JSON.parse(cordova.notification.data);
        let alert = this.alerCtrl.create({
          title: cordova.notification.title,
          subTitle: json.secret
        });
        alert.present();
        })
    })

  }

  getNotifs(){
    this.localNotifications.schedule({
      id: 1,
      text: 'Single Local Notification 1',
      trigger: {at: new Date(new Date().getTime() + 10000)},
      data: { secret: 'secret' },
      foreground: true
    }
    );
    this.localNotifications.schedule({
        id: 2,
        text: 'Single Local Notification 2',
        trigger: {at: new Date(new Date().getTime() + 6000)},
        data: { secret: 'secret' },
        foreground: true
      }
    );
    this.localNotifications.schedule({
        id: 3,
        text: 'Single Local Notification 3',
        trigger: {at: new Date(new Date().getTime() + 3000)},
        data: { secret: 'secret' },
        foreground: true
      }
    );

    // this.localNotifications.isScheduled(1);
  }
  getAll(){
    this.localNotifications.getAll().then(res => {
      this.scheduled = res;
 })
  }
  getNotif(){
    cordova.plugins.notification.local.core.
    fireEvent(this.localNotifications.schedule({
        id: 1,
        text: 'Single Local Notification 1',
        trigger: {at: new Date(new Date().getTime() + 10000)},
        data: { secret: 'secret' }
      }
    ));
    // @ts-ignore
    // this.localNotifications.fireEvent('schedule');
  }

}



